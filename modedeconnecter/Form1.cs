﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace modedeconnecter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Database db = new Database();
        //methode remplir 
        public void RemplirGr()
        {
            if (db.ds.Tables["produit"] != null)
            {
                db.ds.Tables["produit"].Clear();
            }
            db.dap = new SqlDataAdapter("select * from Produit",db.conn);
            db.dap.Fill(db.ds, "produit");
            dataGridView1.DataSource = db.ds.Tables["produit"];
        }
        //remplissage combobox:
        public void RemplirCombo()
        {
            comboBoxCat.Items.Clear();
            db.dap = new SqlDataAdapter("select * from Produit", db.conn);
            db.dap.Fill(db.ds, "produit");
            comboBoxCat.DataSource = db.ds.Tables["produit"];
            comboBoxCat.DisplayMember = "RFERENCE";
            comboBoxCat.ValueMember = "CATEGORIE";
        }
        //Vider :
        public void Vider(Control f)
        {
            foreach (Control ct in f.Controls)
            {
                if (ct.GetType() == typeof(TextBox) || ct.GetType() == typeof(ComboBox))
                {
                    ct.Text = "";

                }

                if (ct.Controls.Count != 0)
                {
                    Vider(ct);
                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

            db.Connecter();
            RemplirCombo();
            RemplirGr();
        }

        private void buttonNOUVEAU_Click(object sender, EventArgs e)
        {

            if (textBoxRef.Text == "" || textBoxINT.Text == "" || comboBoxCat.Text == "" || textBoxPrix.Text == "")
            {
                MessageBox.Show("veuillez remplir tous les champ !");
                return;
            }
            Vider(this);
        }

        private void buttonAJOUTER_Click(object sender, EventArgs e)
        {
            if(textBoxRef.Text=="" || textBoxINT.Text==""|| comboBoxCat.Text == "" || textBoxPrix.Text == "")
            {
                MessageBox.Show("veillez Remplir tous les champs : ");
                return;
            }
            db.ligne = db.ds.Tables["produit"].NewRow();
            db.ligne[0] = textBoxRef.Text;
            db.ligne[1] = textBoxINT.Text;
            db.ligne[2] = comboBoxCat.SelectedValue;
            db.ligne[3] = textBoxPrix.Text;
            for(int i = 0; i < db.ds.Tables["produit"].Rows.Count; i++)
            {
                if (textBoxRef.Text == db.ds.Tables["produit"].Rows[i][0].ToString())
                {
                    MessageBox.Show("produit existe deja ");
                    return;
                }
            }
            db.ds.Tables["produit"].Rows.Add(db.ligne);
            MessageBox.Show("produit ajouter avec succés ");
            dataGridView1.DataSource = db.ds.Tables["produit"];
        }

        private void buttonSUPPRIMER_Click(object sender, EventArgs e)
        {
            if(textBoxRef.Text=="")
            {
                MessageBox.Show("veillez remplir ce champ");
                return;
            }
            bool tr = false;
            for (int i = 0; i < db.ds.Tables["produit"].Rows.Count; i++)
            {
               
                if (textBoxRef.Text == db.ds.Tables["produit"].Rows[i][0].ToString())
                {
                    tr = true;
                    db.ds.Tables["produit"].Rows[i].Delete();
                    MessageBox.Show("produit supprimer avec succée ");
                    dataGridView1.DataSource = db.ds.Tables["produit"];
                    break;
                }

            }
            
            if (tr==false)
            {
                MessageBox.Show("produit n'existe pas");
            }
            
        }

        private void buttonMODIFIER_Click(object sender, EventArgs e)
        {
            if (textBoxRef.Text == "" || textBoxINT.Text == "" || comboBoxCat.Text == "" || textBoxPrix.Text == "")
            {
                MessageBox.Show("veillez Remplir tous les champs : ");
                return;
            }
            bool tr = false;
            for (int i = 0; i < db.ds.Tables["produit"].Rows.Count; i++)
            {

                if (textBoxRef.Text == db.ds.Tables["produit"].Rows[i][0].ToString())
                {
                    tr = true;
                    db.ds.Tables["produit"].Rows[i][1]=textBoxINT.Text;
                    db.ds.Tables["produit"].Rows[i][2]=comboBoxCat.SelectedValue;
                    db.ds.Tables["produit"].Rows[i][3]=textBoxPrix.Text;
                    MessageBox.Show("produit modifier avec succés ");
                    dataGridView1.DataSource = db.ds.Tables["produit"];
                    break;
                }

            }

            if (tr == false)
            {
                MessageBox.Show("produit n'existe pas");
            }
        }

        private void buttonRECHERCHER_Click(object sender, EventArgs e)
        {
            if (db.ds.Tables["produit"] != null)
            {
                db.ds.Tables["produit"].Clear();
            }
            db.dap = new SqlDataAdapter("select REFERENCE,INTITULE,CATEGORIE,PRIX from Produit where REFERENCE = '" + 
                textBoxRef.Text+"'",db.conn);
            db.dap.Fill(db.ds,"produit");
            dataGridView1.DataSource = db.ds.Tables["produit"];
        }

        private void buttonQUITTER_Click(object sender, EventArgs e)
        {
            db.Deconnecter();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //mise a jour automatique 
            SqlCommandBuilder bld = new SqlCommandBuilder(db.dap);
            db.dap.Update(db.ds, "produit");
        }
    }
}
