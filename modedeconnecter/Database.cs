﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace modedeconnecter
{
    class Database
    {
        //declaration des objets sql :
        public SqlConnection conn = new SqlConnection();
        public SqlCommand cmd = new SqlCommand();
        public SqlDataReader dr;
        public DataTable dt = new DataTable();
        public SqlDataAdapter dap = new SqlDataAdapter();
        public DataSet ds = new DataSet();
        public SqlCommandBuilder bc;
        public DataRow ligne;
        // methode connecter : 
        public void Connecter()
        {
            if(conn.State==ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.ConnectionString = @"Data Source=localhost;Initial Catalog=magazin;Integrated Security=True";
                conn.Open();
            }
        }
        //methode deconnecter : 
        public void Deconnecter()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }
    }
}
