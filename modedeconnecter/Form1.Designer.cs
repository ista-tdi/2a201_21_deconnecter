﻿
namespace modedeconnecter
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBoxCat = new System.Windows.Forms.ComboBox();
            this.textBoxPrix = new System.Windows.Forms.TextBox();
            this.textBoxINT = new System.Windows.Forms.TextBox();
            this.textBoxRef = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonMODIFIER = new System.Windows.Forms.Button();
            this.buttonAJOUTER = new System.Windows.Forms.Button();
            this.buttonRECHERCHER = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonQUITTER = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSUPPRIMER = new System.Windows.Forms.Button();
            this.buttonNOUVEAU = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 369);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(1293, 226);
            this.dataGridView1.TabIndex = 5;
            // 
            // comboBoxCat
            // 
            this.comboBoxCat.FormattingEnabled = true;
            this.comboBoxCat.Items.AddRange(new object[] {
            "Electro",
            "Nouriture"});
            this.comboBoxCat.Location = new System.Drawing.Point(332, 145);
            this.comboBoxCat.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.comboBoxCat.Name = "comboBoxCat";
            this.comboBoxCat.Size = new System.Drawing.Size(224, 24);
            this.comboBoxCat.TabIndex = 7;
            // 
            // textBoxPrix
            // 
            this.textBoxPrix.Location = new System.Drawing.Point(332, 203);
            this.textBoxPrix.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxPrix.Name = "textBoxPrix";
            this.textBoxPrix.Size = new System.Drawing.Size(224, 22);
            this.textBoxPrix.TabIndex = 6;
            // 
            // textBoxINT
            // 
            this.textBoxINT.Location = new System.Drawing.Point(332, 98);
            this.textBoxINT.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxINT.Name = "textBoxINT";
            this.textBoxINT.Size = new System.Drawing.Size(224, 22);
            this.textBoxINT.TabIndex = 5;
            // 
            // textBoxRef
            // 
            this.textBoxRef.Location = new System.Drawing.Point(332, 33);
            this.textBoxRef.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.textBoxRef.Name = "textBoxRef";
            this.textBoxRef.Size = new System.Drawing.Size(224, 22);
            this.textBoxRef.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 213);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "PRIX DE VENTE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 158);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "CATEGORIE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "INTITULE";
            // 
            // buttonMODIFIER
            // 
            this.buttonMODIFIER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonMODIFIER.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonMODIFIER.Location = new System.Drawing.Point(39, 196);
            this.buttonMODIFIER.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonMODIFIER.Name = "buttonMODIFIER";
            this.buttonMODIFIER.Size = new System.Drawing.Size(287, 44);
            this.buttonMODIFIER.TabIndex = 3;
            this.buttonMODIFIER.Text = "MODIFIER";
            this.buttonMODIFIER.UseVisualStyleBackColor = false;
            this.buttonMODIFIER.Click += new System.EventHandler(this.buttonMODIFIER_Click);
            // 
            // buttonAJOUTER
            // 
            this.buttonAJOUTER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonAJOUTER.ForeColor = System.Drawing.Color.Lime;
            this.buttonAJOUTER.Location = new System.Drawing.Point(39, 142);
            this.buttonAJOUTER.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonAJOUTER.Name = "buttonAJOUTER";
            this.buttonAJOUTER.Size = new System.Drawing.Size(287, 46);
            this.buttonAJOUTER.TabIndex = 2;
            this.buttonAJOUTER.Text = "AJOUTER";
            this.buttonAJOUTER.UseVisualStyleBackColor = false;
            this.buttonAJOUTER.Click += new System.EventHandler(this.buttonAJOUTER_Click);
            // 
            // buttonRECHERCHER
            // 
            this.buttonRECHERCHER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonRECHERCHER.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.buttonRECHERCHER.Location = new System.Drawing.Point(39, 89);
            this.buttonRECHERCHER.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonRECHERCHER.Name = "buttonRECHERCHER";
            this.buttonRECHERCHER.Size = new System.Drawing.Size(287, 46);
            this.buttonRECHERCHER.TabIndex = 1;
            this.buttonRECHERCHER.Text = "RECHERCHER";
            this.buttonRECHERCHER.UseVisualStyleBackColor = false;
            this.buttonRECHERCHER.Click += new System.EventHandler(this.buttonRECHERCHER_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "REFERENCE";
            // 
            // buttonQUITTER
            // 
            this.buttonQUITTER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonQUITTER.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonQUITTER.Location = new System.Drawing.Point(39, 297);
            this.buttonQUITTER.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonQUITTER.Name = "buttonQUITTER";
            this.buttonQUITTER.Size = new System.Drawing.Size(287, 41);
            this.buttonQUITTER.TabIndex = 5;
            this.buttonQUITTER.Text = "QUITTER";
            this.buttonQUITTER.UseVisualStyleBackColor = false;
            this.buttonQUITTER.Click += new System.EventHandler(this.buttonQUITTER_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(41)))));
            this.groupBox2.Controls.Add(this.buttonQUITTER);
            this.groupBox2.Controls.Add(this.buttonSUPPRIMER);
            this.groupBox2.Controls.Add(this.buttonMODIFIER);
            this.groupBox2.Controls.Add(this.buttonAJOUTER);
            this.groupBox2.Controls.Add(this.buttonRECHERCHER);
            this.groupBox2.Controls.Add(this.buttonNOUVEAU);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox2.Location = new System.Drawing.Point(816, 4);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox2.Size = new System.Drawing.Size(401, 347);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "OPERATION";
            // 
            // buttonSUPPRIMER
            // 
            this.buttonSUPPRIMER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonSUPPRIMER.ForeColor = System.Drawing.Color.Red;
            this.buttonSUPPRIMER.Location = new System.Drawing.Point(39, 249);
            this.buttonSUPPRIMER.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonSUPPRIMER.Name = "buttonSUPPRIMER";
            this.buttonSUPPRIMER.Size = new System.Drawing.Size(287, 39);
            this.buttonSUPPRIMER.TabIndex = 4;
            this.buttonSUPPRIMER.Text = "SUPPRIMER";
            this.buttonSUPPRIMER.UseVisualStyleBackColor = false;
            this.buttonSUPPRIMER.Click += new System.EventHandler(this.buttonSUPPRIMER_Click);
            // 
            // buttonNOUVEAU
            // 
            this.buttonNOUVEAU.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(41)))), ((int)(((byte)(50)))));
            this.buttonNOUVEAU.ForeColor = System.Drawing.Color.Fuchsia;
            this.buttonNOUVEAU.Location = new System.Drawing.Point(39, 39);
            this.buttonNOUVEAU.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonNOUVEAU.Name = "buttonNOUVEAU";
            this.buttonNOUVEAU.Size = new System.Drawing.Size(287, 39);
            this.buttonNOUVEAU.TabIndex = 0;
            this.buttonNOUVEAU.Text = "NOUVEAU";
            this.buttonNOUVEAU.UseVisualStyleBackColor = false;
            this.buttonNOUVEAU.Click += new System.EventHandler(this.buttonNOUVEAU_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(45)))), ((int)(((byte)(60)))));
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.comboBoxCat);
            this.groupBox1.Controls.Add(this.textBoxPrix);
            this.groupBox1.Controls.Add(this.textBoxINT);
            this.groupBox1.Controls.Add(this.textBoxRef);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Location = new System.Drawing.Point(17, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.groupBox1.Size = new System.Drawing.Size(692, 336);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PRODUIT";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(26, 286);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(241, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 610);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxCat;
        private System.Windows.Forms.TextBox textBoxPrix;
        private System.Windows.Forms.TextBox textBoxINT;
        private System.Windows.Forms.TextBox textBoxRef;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonMODIFIER;
        private System.Windows.Forms.Button buttonAJOUTER;
        private System.Windows.Forms.Button buttonRECHERCHER;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonQUITTER;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSUPPRIMER;
        private System.Windows.Forms.Button buttonNOUVEAU;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
    }
}

