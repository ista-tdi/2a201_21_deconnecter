create database magazin;
go
use magazin;
go
create table Produit(
	REFERENCE int primary key not null,
	INTITULE varchar(50) not null,
	CATEGORIE varchar(50) not null,
	PRIX float not null
);
go
insert into Produit values(1,'Iphone 6','Electro',4000);
insert into Produit values(2,'Couscous','Nouriture',85);
insert into Produit values(3,'Ipad','Electro',1500);
insert into Produit values(4,'Pitza','Nouriture',50);
insert into Produit values(5,'Jus','Nouriture',20);